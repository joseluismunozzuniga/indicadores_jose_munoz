'use strict';
const fs = require('fs'); //importamos FileSystem
const fetch = require('node-fetch');
const estadisticas = require('./estadisticas');
const status = response => {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  }
  return Promise.reject(new Error(response.status.Text));
};
const obtenerJson = response => {
  return response.json();
};

let list = [];
let almacenarArchivos = [];
const SolicitarDatos = () => {
  return new Promise((resolve, reject) => {
    fetch('https://mindicador.cl/api')
      .then(status)
      .then(obtenerJson)
      .then(res => {
        let list = [];
        let dolar = res.dolar;
        let euro = res.euro;
        let tasa_desempleo = res.tasa_desempleo;
        let tiempo = new Date().getTime();
        list.push({ dolar, euro, tasa_desempleo, tiempo });
        let data = JSON.stringify(list, null, 2);
        fs.writeFile(`datos/datos${list[0].tiempo}.ind`, data, (err) => {
          if (err) throw new Error('error al grabar los datos', err);
        });
        resolve(res);
      })
      .catch(false);



  });


}


/** Para promediar los datos 
 * 1.almacenar los archivos del directorio
 * 2.acceder a los datos de cada archivo
 * 3.calcular promedio
 * 
 */

const promediar = (opcionPromedio) => {
  return new Promise((resolve, reject) => {
      leerDirectorio()
      .then(estadisticas.calcularPromedio(almacenarArchivos))
      .then(resolve(true))
      
  });
}
const leerDirectorio = () => {
  almacenarArchivos = [];
  return new Promise((resolve, reject) => {
    fs.readdirSync('./datos/').forEach(file => {
      let fileName = `datos/${file}`;
      let content = fs.readFileSync(fileName, "utf8");
      content = JSON.parse(content);
      almacenarArchivos.push(content[0]);

    });
    resolve(true);
  })
}


function buscarActual() {
  let orderArray = almacenarArchivos.sort((a, b) => b.tiempo - a.tiempo);
  console.log("la fecha actual es :");
  console.log(orderArray[0].dolar.fecha);
  console.log("valores recientes son: ");
  console.log("Dolar: " + orderArray[0].dolar.valor);
  console.log("Euro: " + orderArray[0].euro.valor);
  console.log("Tasa desempleo: " + orderArray[0].tasa_desempleo.valor);

}
const actual = () => {
  return new Promise((resolve, reject) => {
    leerDirectorio().then(
      buscarActual()
    ).then(resolve(true))

  })
}
const minimo = () => {
  return new Promise((resolve, reject) => {
    leerDirectorio().then(

      estadisticas.buscarMinimo(almacenarArchivos)



    ).then(resolve(true))
  })
}

const maximo = () => {
  return new Promise((resolve, reject) => {
    leerDirectorio().then(

      estadisticas.buscarMaximo(almacenarArchivos)

    ).then(resolve(true))
  })
}
module.exports = {
  SolicitarDatos,
  promediar,
  actual,
  minimo,
  maximo
}